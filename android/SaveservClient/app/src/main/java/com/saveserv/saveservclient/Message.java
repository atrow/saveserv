package com.saveserv.saveservclient;
public class Message {

    public enum MessageType {
        LOGIN,OTHER,GPS;
    }
    public enum RoleItem {
        PRODUSER, CONSUMER;
    }

    private String login;
    private String password;
    private String itemName;
    private RoleItem role;
    private MessageType type;

    public Message( MessageType type, String login, String password, String itemName, RoleItem role) {
        this.login = login;
        this.password = password;
        this.itemName = itemName;
        this.role = role;
        this.type = type;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Message{");
        sb.append("login='").append(login).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append(", itemName='").append(itemName).append('\'');
        sb.append(", role=").append(role);
        sb.append(", type=").append(type);
        sb.append('}');
        return sb.toString();
    }
}


