package com.saveserv.saveservclient;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class RegistrationActivity extends ActionBarActivity {

    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        if(isMyServiceRunning(TrackingService.class)){
//            Intent trackingActivityIntent = new Intent(RegistrationActivity.this, TrackingActivity.class);
//            startActivity(trackingActivityIntent);
//            finish();
//            return;
//        }

        setContentView(R.layout.activity_registration);
        final EditText loginField = (EditText) findViewById(R.id.login);
        final EditText passwordField = (EditText) findViewById(R.id.password);
        Button start_track = (Button) findViewById(R.id.start_tracking_btn);

        start_track.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String login = loginField.getText().toString();
                String password = passwordField.getText().toString();
                if (login != null && password != null) {
                    Intent trackingActivityIntent = new Intent(RegistrationActivity.this, TrackingActivity.class);
                    trackingActivityIntent.putExtra("login", login);
                    trackingActivityIntent.putExtra("password", password);

                    PreferencesUtil.saveStringPreferences(getApplicationContext(), LOGIN, login);
                    PreferencesUtil.saveStringPreferences(getApplicationContext(), PASSWORD, password);
                    startActivity(trackingActivityIntent);
                    finish();
                }
            }
        });
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_registration, menu);
        return true;
    }
}
