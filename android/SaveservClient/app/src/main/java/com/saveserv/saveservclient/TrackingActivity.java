package com.saveserv.saveservclient;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.widget.CompoundButton;
import android.widget.ToggleButton;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;

public class TrackingActivity extends ActionBarActivity {
    private String mLogin;
    private String mPassword;
    double mLatitude;
    double mLongitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_container);

        final ToggleButton toggleButton= (ToggleButton) findViewById(R.id.turn);
        Bundle bundle = getIntent().getExtras();
        double latitude = 0;
        double longitude = 0;

        if (isMyServiceRunning(TrackingService.class)) toggleButton.setChecked(true);

        if (bundle != null ){
            mLogin =  bundle.getString("login");
            mPassword = bundle.getString("password");
            longitude = bundle.getDouble("longitude");
            latitude = bundle.getDouble("latitude");
        }

        final GoogleMap map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        if (longitude != 0 || latitude != 0){
            LatLng latLng = new LatLng(latitude, longitude);
            Log.i("+", "++++" + longitude + " " + latitude);
            map.addMarker(new MarkerOptions().position(latLng).title("car"));
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
            map.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
        }

        SmartLocation.with(this).location()
                .oneFix()
                .start(new OnLocationUpdatedListener() {
                    @Override
                    public void onLocationUpdated(Location location) {
                        double longitude = location.getLongitude();
                        double latitude = location.getLatitude();
                        mLongitude = longitude;
                        mLatitude = latitude;

                        LatLng latLng = new LatLng(mLatitude, mLongitude);

                        Log.i("+", "++++" + mLongitude + " " + mLatitude);
                        map.addMarker(new MarkerOptions().position(latLng).title("user").alpha(0.5f));
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                        map.animateCamera(CameraUpdateFactory.zoomTo(13), 2000, null);
                    }
                });

        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Log.i("+", "start");
                    Intent serviceIntent = new Intent(TrackingActivity.this, TrackingService.class);
                    serviceIntent.putExtra("login", mLogin);
                    serviceIntent.putExtra("password", mPassword);
                    startService(serviceIntent);
                } else {
                    Log.i("+", "stop");
                    stopService(new Intent(TrackingActivity.this, TrackingService.class));
                }
            }
        });
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_registration, menu);
        return true;
    }
}
