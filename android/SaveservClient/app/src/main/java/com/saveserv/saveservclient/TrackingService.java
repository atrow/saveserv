package com.saveserv.saveservclient;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import com.google.gson.Gson;
import com.safeserv.client.MessageHandler;
import com.safeserv.client.MinaClient;
import org.json.JSONException;
import org.json.JSONObject;

public class TrackingService extends Service {

    public static final String HOST = "safeserv.cloudapp.net";
    public static final int PORT = 8878;

    double mLatitude;
    double mLongitude;
    private String mLogin;
    private String mPassword;
    private MinaClient minaClient;
    private long mLastAlertTime = 0;
    private static final int delayBetweenAlert = 5000;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("+", "onStartCommand");

        mLogin = PreferencesUtil.readStringPreferences(getApplicationContext(), RegistrationActivity.LOGIN, null);
        mPassword = PreferencesUtil.readStringPreferences(getApplicationContext(), RegistrationActivity.PASSWORD, null);

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i("+", "onCreate");
        connectToTheServer(mLogin, mPassword);
    }

    @Override
    public void onDestroy() {
        Log.i("+", "onDestroy");
        super.onDestroy();
    }

    private void connectToTheServer(String login, String password) {
        Log.i("+", "connectToTheServer");
        new Thread(new ServerThread(login, password)).start();
    }

    public class ServerThread implements Runnable {
        String login;
        String password;

        public ServerThread(String login, String password) {
            this.login = login;
            this.password = password;
        }

        public void run() {
            Log.i("+", "start connecting");
            Gson gson = new Gson();
            final Message message = new Message(Message.MessageType.LOGIN, mLogin, mPassword, "ANDROID DEVICE", Message.RoleItem.CONSUMER);
            minaClient = new MinaClient(HOST, PORT, gson.toJson(message));
            minaClient.setMessageHandler(new MessageHandler() {
                @Override
                public void onMessage(Object o) {
                    String responseString = (String) o;
                    Log.i("+", "response: " + responseString);
                    // {"type":"ALERT","latitude":"50.4679711","longitude":"30.4622947","item":"android"}
                    if (responseString.contains("ALERT")){
                        Log.i("+", "ALERT!!!!!");
                        long currentTime = System.currentTimeMillis();
                        if ((currentTime - mLastAlertTime) > delayBetweenAlert){
                            mLastAlertTime = currentTime;
                            try {
                                JSONObject jsonObject = new JSONObject(responseString);
                                mLongitude = jsonObject.getDouble("longitude");
                                mLatitude = jsonObject.getDouble("latitude");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            notifyUser();
                        }
                    }
                }
            });
            minaClient.connect();
        }
    }

    private void notifyUser() {
        Log.i("+", "notify user");
        Intent intent = new Intent(TrackingService.this, TrackingActivity.class);
        intent.putExtra("longitude", mLongitude);
        intent.putExtra("latitude", mLatitude);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        final NotificationManagerCompat nm = NotificationManagerCompat.from(this);
        final NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setTicker("Your car theft")
                .setContentTitle("Alert")
                .setContentText("Your car theft")
                .setSmallIcon(android.R.drawable.sym_def_app_icon)
                .setWhen(System.currentTimeMillis())
                //.setSound(notificationSound)
                .setContentIntent(pendingIntent);

//
//        if (bannerBitmap != null) {
//            final NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();
//            bigPictureStyle.bigPicture(bannerBitmap);
//            bigPictureStyle.setSummaryText(message);
//            builder.setStyle(bigPictureStyle);
//        }

        Notification notification = builder.build();
        notification.defaults |= Notification.DEFAULT_LIGHTS;
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        //notification.sound = Uri.parse("android.resource://"
        //        + this.getPackageName() + "/" + R.raw.alarm);
        nm.notify(0, notification);
    }
}
