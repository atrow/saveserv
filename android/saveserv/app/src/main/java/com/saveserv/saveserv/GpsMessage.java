package com.saveserv.saveserv;

public class GpsMessage {

    MessageType type;
    String item;
    double longitude;
    double latitude;

    public GpsMessage(MessageType type, String item, double longitude, double latitude) {
        this.type = type;
        this.item = item;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("GpsMessage{");
        sb.append("type=").append(type);
        sb.append(", item='").append(item).append('\'');
        sb.append(", longitude=").append(longitude);
        sb.append(", latitude=").append(latitude);
        sb.append('}');
        return sb.toString();
    }
}
