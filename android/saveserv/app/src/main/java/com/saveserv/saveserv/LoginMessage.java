package com.saveserv.saveserv;
public class LoginMessage {

    public enum RoleItem {
        PRODUSER, CONSUMER;
    }

    private MessageType type;
    private String login;
    private String password;
    private String itemName;
    private RoleItem role;

    public LoginMessage(MessageType type, String login, String password, String itemName, RoleItem role) {
        this.type = type;
        this.login = login;
        this.password = password;
        this.itemName = itemName;
        this.role = role;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Message{");
        sb.append("type=").append(type);
        sb.append(", login='").append(login).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append(", itemName='").append(itemName).append('\'');
        sb.append(", role=").append(role);
        sb.append('}');
        return sb.toString();
    }
}


