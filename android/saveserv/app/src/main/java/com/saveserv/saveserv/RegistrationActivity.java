package com.saveserv.saveserv;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;


public class RegistrationActivity extends ActionBarActivity {

    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        final EditText loginField = (EditText) findViewById(R.id.login);
        final EditText passwordField = (EditText) findViewById(R.id.password);
        Button start_track = (Button) findViewById(R.id.start_tracking_btn);

        start_track.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String login = loginField.getText().toString();
                String password = passwordField.getText().toString();
                if (login != null && password !=null){

                    Intent trackingActivityIntent = new Intent(RegistrationActivity.this, TrackingActivity.class);
                    trackingActivityIntent.putExtra(LOGIN, login);
                    trackingActivityIntent.putExtra(PASSWORD, password);

                    PreferencesUtil.saveStringPreferences(getApplicationContext(), LOGIN, login);
                    PreferencesUtil.saveStringPreferences(getApplicationContext(), PASSWORD, password);

                    startActivity(trackingActivityIntent);
                    finish();
                }
            }
        });
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.reg_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
