package com.saveserv.saveserv;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

public class TrackingActivity extends ActionBarActivity{

    private String mLogin;
    private String mPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null ){
            mLogin =  bundle.getString("login");
            mPassword = bundle.getString("password");
        }

        ToggleButton toggleButton = (ToggleButton) findViewById(R.id.turn_btn);
        toggleButton.setChecked(false);
        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!isChecked){
                    stopService(new Intent(TrackingActivity.this, TrackingService.class));
                }else{
                    Intent serviceIntent = new Intent(TrackingActivity.this, TrackingService.class);
                    serviceIntent.putExtra("login", mLogin);
                    serviceIntent.putExtra("password", mPassword);
                    startService(serviceIntent);
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.reg_menu, menu);
        return true;
    }
}
