package com.saveserv.saveserv;

import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.IBinder;
import android.util.Log;
import com.google.gson.Gson;
import com.safeserv.client.MessageHandler;
import com.safeserv.client.MinaClient;
import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;

public class TrackingService extends Service implements SensorEventListener {
    public static final String HOST = "safeserv.cloudapp.net";
    public static final int PORT = 8878;
    public static final double DELTA = 0.4;

    String item;
    double mLatitude;
    double mLongitude;
    private String mLogin;
    private String mPassword;
    private long mStartTime;
    private float[] mGravity = new float[3];
    private SensorManager mSensorManager;
    private MinaClient minaClient;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("+", "onStartCommand");

        mLogin = PreferencesUtil.readStringPreferences(getApplicationContext(),RegistrationActivity.LOGIN, null);
        mPassword = PreferencesUtil.readStringPreferences(getApplicationContext(),RegistrationActivity.PASSWORD, null);

        Log.i("+", mLogin+mPassword);
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        return START_STICKY_COMPATIBILITY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i("+", "onCreate");
        mStartTime = System.currentTimeMillis();
        connectToTheServer(mLogin, mPassword);
    }



    @Override
    public void onDestroy() {
        Log.i("+", "onDestroy");
        mSensorManager.unregisterListener(this);
        super.onDestroy();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            long actualTime = System.currentTimeMillis();

            float rawX = event.values[0];
            float rawY = event.values[1];
            float rawZ = event.values[2];
            mGravity[0] = lowPass(rawX, mGravity[0]);
            mGravity[1] = lowPass(rawY, mGravity[1]);
            mGravity[2] = lowPass(rawZ, mGravity[2]);

            Log.i("+", "ROW VALUES: " + " x =" + rawX + " y = " + rawY + " z = " + rawZ);
            Log.i("+", "FILTERED VALUES: " + " x =" + mGravity[0] + " y = " + mGravity[1] + " z = " + mGravity[2]);

            if ((Math.abs(rawX) - mGravity[0]) > DELTA
                    || (Math.abs(rawY) - mGravity[1]) > DELTA
                    || (Math.abs(rawZ) - mGravity[2]) > DELTA) {

                if ((actualTime - mStartTime) > 5000) {
                    Log.i("+", "ALARM!");
                    SmartLocation.with(TrackingService.this).location()
                            .oneFix()
                            .start(new OnLocationUpdatedListener() {
                                @Override
                                public void onLocationUpdated(Location location) {
                                    double longitude = location.getLongitude();
                                    double latitude = location.getLatitude();
                                    mLongitude = longitude;
                                    mLatitude = latitude;
                                }
                            });
                    GpsMessage gpsData = new GpsMessage(MessageType.GPS, item, mLongitude, mLatitude);
                    Gson gson = new Gson();
                    minaClient.sendAlert(gson.toJson(gpsData));
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    private float lowPass(float current, float gravity) {
        float mAlpha = 0.8f;
        return gravity * mAlpha + current * (1 - mAlpha);
    }

    private void connectToTheServer(String login, String password) {
        Log.i("+", "connectToTheServer");
        new Thread(new ServerThread(login, password)).start();
    }

    public class ServerThread implements Runnable {
        String login;
        String password;

        public ServerThread(String login, String password) {
            this.login = login;
            this.password = password;
        }

        public void run() {
            Log.i("+", "start connecting");
            item="ANDROID DEVICE";
            LoginMessage loginMessage = new LoginMessage(MessageType.LOGIN, mLogin, mPassword, item, LoginMessage.RoleItem.PRODUSER);

            Gson gson = new Gson();
            minaClient = new MinaClient(HOST, PORT, gson.toJson(loginMessage));
            minaClient.connect();
            minaClient.setMessageHandler(new MessageHandler() {
                @Override
                public void onMessage(Object o) {
                    Log.i("+", (String) o);
                }
            });
        }
    }
}
