package com.safeserv.client;

/**
 *
 */
public interface MessageHandler {

    void onMessage(Object message);

}
