package com.safeserv.client;


import org.apache.mina.core.RuntimeIoException;
import org.apache.mina.core.future.CloseFuture;
import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.core.write.WriteRequest;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;
import org.apache.mina.filter.logging.LoggingFilter;
import org.apache.mina.transport.socket.nio.NioSocketConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.nio.charset.Charset;

/**
 *
 */
public class MinaClient {

    final static Logger log = LoggerFactory.getLogger(MinaClient.class);

    private String hostname;

    private int port;

    private String loginString;

    private NioSocketConnector connector;

    private ConnectFuture connectFuture;
    private MessageHandler messageHandler;

    public MinaClient(String hostname, Integer port, String loginString) {
        this.hostname = hostname;
        this.port = port;
        this.loginString = loginString;
    }

    public boolean isConnected() {
        return connectFuture != null &&
                connectFuture.awaitUninterruptibly().isConnected();
    }

    public void sendAlert(String message) {

        log.debug(String.format("Sending new alert: '%s'", message));

        IoSession session = connectFuture.awaitUninterruptibly().getSession();
        session.write(message);
    }

    public void setMessageHandler(MessageHandler messageHandler) {
        this.messageHandler = messageHandler;
    }

    public void connect() {

        log.debug(String.format("Establishing new connection to '%s' port %s", hostname, port));

        connector = new NioSocketConnector();
        connector.setConnectTimeoutMillis(2000);

        connector.getFilterChain().addLast("codec",
                new ProtocolCodecFilter(new TextLineCodecFactory(Charset.forName("UTF-8"), "\n", "\n"))
        );
        connector.getFilterChain().addLast("logger", new LoggingFilter());

        connector.setHandler(new MinaClientSessionHandler(loginString, messageHandler));

        for (int i = 0; i < 3; i++) {
            try {
                connectFuture = connector.connect(new InetSocketAddress(hostname, port));
                connectFuture.awaitUninterruptibly();
                WriteRequest req = connectFuture.getSession().getCurrentWriteRequest();
                if (req != null) {
                    req.getFuture().awaitUninterruptibly();
                }
                break;
            } catch (RuntimeIoException e) {
                log.error("Failed to connect to server", e);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e1) {
                    throw new RuntimeException(e);
                }
            }
        }

        log.info(String.format("Successful connection to '%s' port %s", hostname, port));
    }

    public void disconnect() {

        log.debug("Closing connection");

        connectFuture.awaitUninterruptibly();
        IoSession session = connectFuture.getSession();

        CloseFuture closeFuture = session.getCloseFuture();
        try {
            if (!closeFuture.await(1000)) {
                log.warn("Forcing session to close");
                session.close(true);
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        // wait until the summation is done
        session.getCloseFuture().awaitUninterruptibly();
        connector.dispose();

        log.info("Connection closed");
    }
}
