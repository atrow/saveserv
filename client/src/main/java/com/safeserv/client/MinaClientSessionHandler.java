package com.safeserv.client;

import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class MinaClientSessionHandler extends IoHandlerAdapter {

    final static Logger log = LoggerFactory.getLogger(MinaClientSessionHandler.class);

    private String loginString;
    private MessageHandler messageHandler;

    public MinaClientSessionHandler(String loginString, MessageHandler messageHandler) {
        this.loginString = loginString;
        this.messageHandler = messageHandler;
    }

    @Override
    public void sessionOpened(IoSession session) throws Exception {
        log.debug("Client is Logging in");
        session.write(loginString);
    }

    @Override
    public void messageReceived(IoSession session, Object message) throws Exception {
        log.debug("Got message from server: " + message);
        if (messageHandler != null) {
            messageHandler.onMessage(message);
        }
    }

}
