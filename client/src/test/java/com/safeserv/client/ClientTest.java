package com.safeserv.client;

import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;
import org.apache.mina.filter.logging.LoggingFilter;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.nio.charset.Charset;

import static org.junit.Assert.*;

/**
 * Test for Socket client
 */
public class ClientTest {

    public static final int PORT = 2878;

    final static Logger log = LoggerFactory.getLogger(ClientTest.class);

    private IoAcceptor server;

    private MinaClient client;

    private String loginString;

    @Test
    public void testLogin() throws Exception {

        server.setHandler(new IoHandlerAdapter() {
            @Override
            public void messageReceived(IoSession session, Object message) throws Exception {

                log.info("Got message: " + message);
                assertEquals(loginString, message);
                session.close(false);
            }
        });

        server.bind(new InetSocketAddress(PORT));
        client.connect();
    }

    @Test
    public void testAlert() throws Exception {

        final String alertText = "Alert";

        server.setHandler(new IoHandlerAdapter() {
            int messageCounter = 0;
            boolean loginReceived = false;
            boolean alertReceived = false;

            @Override
            public void messageReceived(IoSession session, Object message) throws Exception {
                log.info("Got message: " + message);

                if (message.equals(loginString)) {
                    loginReceived = true;
                } else if (message.equals(alertText)) {
                    alertReceived = true;
                } else {
                    fail("Received unexpected thing: " + message);
                }

                if (messageCounter++ > 1) {
                    assertTrue("Didn't receive login", loginReceived);
                    assertTrue("Didn't receive alert", alertReceived);
                }
            }
        });

        server.bind(new InetSocketAddress(PORT));

        client.connect();
        client.sendAlert(alertText);
    }

    @Test
    public void testLoginAndMessageToClient() throws Exception {

        final String serverMessage = "Message";

        server.setHandler(new IoHandlerAdapter() {
            @Override
            public void messageReceived(IoSession session, Object message) throws Exception {

                log.info("Got message: " + message);
                assertEquals(loginString, message);

                session.write(serverMessage);
            }
        });

        client.setMessageHandler(new MessageHandler() {
            @Override
            public void onMessage(Object message) {
                log.info("Got message: " + message);
                assertEquals(serverMessage, message);
            }
        });

        server.bind(new InetSocketAddress(PORT));
        client.connect();
    }

    @Before
    public void before() throws Exception {

        loginString = "Login";

        client = new MinaClient("localhost", PORT, loginString);

        server = new NioSocketAcceptor();
        server.getFilterChain().addLast("codec",
                new ProtocolCodecFilter(new TextLineCodecFactory(Charset.defaultCharset(), System.lineSeparator(), System.lineSeparator()))
        );

        server.getFilterChain().addLast("logger", new LoggingFilter());
    }

    @After
    public void after() throws Exception {

        client.disconnect();

        server.unbind();
        server.dispose();
    }

}
