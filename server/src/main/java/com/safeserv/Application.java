package com.safeserv;

import com.safeserv.server.MinaServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class Application {

    final static Logger log = LoggerFactory.getLogger(Application.class);

    final static Integer port = 8878;

    public static void main(String[] args) {

        log.info(" ### --- STARTING SafeServ --- ####");

        MinaServer server = new MinaServer(port);
        server.start();
    }

}
