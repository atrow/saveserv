package com.safeserv.domain;

/**
 *
 */
public class Alert {

    private final String type = "ALERT";

    private String latitude;

    private String longitude;

    private String item;

    public Alert(String latitude, String longitude, String item) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.item = item;
    }

    public String getType() {
        return type;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    @Override
    public String toString() {
        return "Alert{" +
                "latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", item='" + item + '\'' +
                '}';
    }
}