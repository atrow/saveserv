package com.safeserv.domain;

/**
 * Created by free on 18.04.15.
 */
public class LoginData {

    private String login;
    private String password;
    private String itemName;
    private RoleItem role;

    public LoginData(String login, String password, String itemName, RoleItem role) {
        this.login = login;
        this.password = password;
        this.itemName = itemName;
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public RoleItem getRole() {
        return role;
    }

    public void setRole(RoleItem role) {
        this.role = role;
    }

    public String getToken() {
        return login + ":" + password;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("LoginData{");
        sb.append("login='").append(login).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append(", itemName='").append(itemName).append('\'');
        sb.append(", role=").append(role);
        sb.append('}');
        return sb.toString();
    }
}
