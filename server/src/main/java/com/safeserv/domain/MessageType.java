package com.safeserv.domain;

/**
 * Created by free on 18.04.15.
 */
public enum MessageType {
    LOGIN, GPS, OTHER;
}
