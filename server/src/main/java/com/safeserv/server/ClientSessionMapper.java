package com.safeserv.server;

import org.apache.mina.core.session.IoSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 *
 */
public class ClientSessionMapper {

    final static Logger log = LoggerFactory.getLogger(ClientSessionMapper.class);

    Map<IoSession, String> sessionToConsumerMap = new HashMap<>();

    Map<String, Set<IoSession>> consumerToSessionsMap = new HashMap<>();

    Map<IoSession, String> sessionToProducerMap = new HashMap<>();

    public void removeSession(IoSession session) {
        log.debug(String.format("Removing session with id: %s", session.getId()));

        String token = sessionToConsumerMap.get(session);
        if (consumerToSessionsMap.containsKey(token)) {
            consumerToSessionsMap.get(token).remove(session);
        }

        sessionToConsumerMap.remove(session);
        sessionToProducerMap.remove(session);
    }

    public void addConsumerSession(String token, IoSession session) {
        log.debug(String.format("Adding Consumer with token %s, session id: %s", token, session.getId()));

        sessionToConsumerMap.put(session, token);

        if (!consumerToSessionsMap.containsKey(token)) {
            consumerToSessionsMap.put(token, new HashSet<IoSession>());
        }
        consumerToSessionsMap.get(token).add(session);
    }

    public void addProducerSession(String token, IoSession session) {
        log.debug(String.format("Adding Producer with token %s, session id: %s", token, session.getId()));
        sessionToProducerMap.put(session, token);
    }

    public Collection<IoSession> getConsumerSessionsForProducer(IoSession session) {
        log.debug(String.format("Getting Producer by session id: %s", session.getId()));
        String token = sessionToProducerMap.get(session);
        log.debug(String.format("Found: %s", token));

        if (token == null) {
            log.error(String.format("Producer not found for session: %s", session.getId()));
            return Collections.EMPTY_LIST;
        }

        if (!consumerToSessionsMap.containsKey(token)) {
            return Collections.EMPTY_LIST;
        }

        return consumerToSessionsMap.get(token);
    }
}
