package com.safeserv.server;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.safeserv.domain.Alert;
import com.safeserv.domain.Gps;
import com.safeserv.domain.LoginData;
import com.safeserv.domain.RoleItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Type;
import java.util.Map;

/**
 *
 */
public class MessageParser {

    final static Logger log = LoggerFactory.getLogger(MessageParser.class);

    private Gson gson = new Gson();

    public Map<String, String> parseMessage(Object raw) {
        Type mapOfStringObjectType = new TypeToken<Map<String, String>>() {}.getType();
        return parseOrNull(raw, mapOfStringObjectType);
    }

    private <T> T parseOrNull(Object raw, Type type) {

        try {
            return gson.fromJson(raw.toString(), type);
        } catch (JsonSyntaxException e) {
            log.error(String.format("Wrong message format: %s", raw), e);
        }
        return null;
    }

    public LoginData mapLoginData(Map<String, String> map) {
        LoginData loginData = new LoginData(
                map.get("login"),
                map.get("password"),
                map.get("itemName"),
                RoleItem.valueOf(map.get("role"))
        );
        return isValid(loginData) ? loginData : null;
    }

    private boolean isValid(LoginData loginData) {
        boolean valid = !(loginData.getLogin() == null ||
                          loginData.getPassword() == null ||
                          loginData.getItemName() == null);
        if (!valid) {
            log.error("Invalid LoginData request: " + loginData.toString());
        }
        return valid;
    }

    public Gps mapGps(Map<String, String> map) {
        return new Gps(
                map.get("latitude"),
                map.get("longitude")
        );
    }

    public String mapAlert(String itemName, Gps gps) {
        Alert alert = new Alert(
                gps.getLatitude(),
                gps.getLongitude(),
                itemName
        );

        return gson.toJson(alert);
    }
}
