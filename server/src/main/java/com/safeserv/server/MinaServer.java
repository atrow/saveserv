package com.safeserv.server;

import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;
import org.apache.mina.filter.logging.LoggingFilter;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.charset.Charset;

/**
 *
 */
public class MinaServer {

    final static Logger log = LoggerFactory.getLogger(MinaServer.class);

    private int port;

    private IoAcceptor acceptor;


    public MinaServer(int port) {
        this.port = port;
    }

    public void start() {

        log.debug(String.format("Starting server on port %s", port));

        acceptor = new NioSocketAcceptor();
        acceptor.getFilterChain().addLast("codec",
                new ProtocolCodecFilter(new TextLineCodecFactory(Charset.forName("UTF-8"), "\n", "\n"))
        );
        acceptor.getFilterChain().addLast("logger", new LoggingFilter());

        acceptor.setHandler(new MinaServerHandler());

        try {
            acceptor.bind(new InetSocketAddress(port));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        log.info(String.format("Successfully started on port %s", port));
    }

    public void stop() {
        acceptor.unbind();
        acceptor.dispose();
    }

}
