package com.safeserv.server;

import com.safeserv.domain.Gps;
import com.safeserv.domain.LoginData;
import com.safeserv.domain.MessageType;
import com.safeserv.domain.RoleItem;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Map;

/**
 *
 */
public class MinaServerHandler extends IoHandlerAdapter {

    final static Logger log = LoggerFactory.getLogger(MinaServerHandler.class);
    public static final String OK = "OK";
    public static final String FAIL = "FAIL";

    private MessageParser parser = new MessageParser();

    private ClientSessionMapper clientSessionMapper = new ClientSessionMapper();

    @Override
    public void sessionOpened(IoSession session) throws Exception {
        log.debug(String.format("New Client connected, IP: %s SID: %s", session.getRemoteAddress(), session.getId()));
    }

    @Override
    public void sessionClosed(IoSession session) throws Exception {
        handleLogout(session);
    }

    @Override
    public void messageReceived(IoSession session, Object clientMessage) throws Exception {

        Map<String, String> map = parser.parseMessage(clientMessage);
        if (map == null) {
            session.write(FAIL);
            return;
        }

        MessageType type = MessageType.valueOf(map.get("type"));
        switch (type) {
            case LOGIN:
                handleLogin(session, map);
                break;
            case GPS:
                handleGps(session, map);
                break;
            case OTHER:
            default:
                session.write(FAIL);
                log.error(String.format("Unexpected message of type %s: '%s'", type, clientMessage));
        }
    }

    private void handleGps(IoSession session, Map<String, String> map) {
        Gps gps = parser.mapGps(map);
        if (gps == null) {
            session.write(FAIL);
            return;
        }

        Collection<IoSession> consumerSessions = clientSessionMapper.getConsumerSessionsForProducer(session);
        for (IoSession consumerSession : consumerSessions) {

            relayGpsMessage(gps, session, consumerSession);
        }

        session.write(OK);
    }

    private void relayGpsMessage(Gps gps, IoSession producerSession, IoSession consumerSession) {

        String itemName = (String) producerSession.getAttribute("itemName");

        String message = parser.mapAlert(itemName, gps);
        consumerSession.write(message);
    }

    private void handleLogin(IoSession session, Map<String, String> map) {
        LoginData loginData = parser.mapLoginData(map);
        if (loginData == null) {
            session.write(FAIL);
            return;
        }

        session.setAttribute("login", loginData.getLogin());
        session.setAttribute("itemName", loginData.getItemName());
        session.setAttribute("role", loginData.getRole());

        RoleItem role = loginData.getRole();
        if (role.equals(RoleItem.CONSUMER)) {
            clientSessionMapper.addConsumerSession(loginData.getToken(), session);
        } else if(role.equals(RoleItem.PRODUSER)) {
            clientSessionMapper.addProducerSession(loginData.getToken(), session);
        }

        session.write(OK);
    }

    private void handleLogout(IoSession session) {
        clientSessionMapper.removeSession(session);
    }

}
