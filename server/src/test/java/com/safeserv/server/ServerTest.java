package com.safeserv.server;

import com.safeserv.client.MessageHandler;
import com.safeserv.client.MinaClient;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test for Socket server
 */
public class ServerTest {

    final static Logger log = Logger.getLogger(ServerTest.class);

    private static final int PORT = 1787;

    private static final String LOGIN_PRODUCER_STRING =
            "{\"type\": \"LOGIN\",\"login\": \"Free\",\"password\": \"1111\",\"itemName\": \"android\",\"role\": \"PRODUSER\"}";

    private static final String LOGIN_CONSUMER_STRING =
            "{\"type\": \"LOGIN\",\"login\": \"Free\",\"password\": \"1111\",\"itemName\": \"android\",\"role\": \"CONSUMER\"}";

    private static final String GPS_STRING =
            "{\"type\": \"GPS\",\"latitude\": \"50.4679711\",\"longitude\": \"30.4622947\"}";

    private MinaServer server;

    private MinaClient producerClient;
    private MinaClient consumerClient;


    @Before
    public void before() throws Exception {

        server = new MinaServer(PORT);
        server.start();

        producerClient = new MinaClient("localhost", PORT, LOGIN_PRODUCER_STRING);
        consumerClient = new MinaClient("localhost", PORT, LOGIN_CONSUMER_STRING);
    }

    @After
    public void after() throws Exception {
        server.stop();
    }

    @Test
    public void testConnect() throws Exception {

        producerClient.connect();
        Thread.sleep(1000);
    }

    @Test
    public void testConnectAndGps() throws Exception {

        producerClient.connect();
        Thread.sleep(1000);
        producerClient.sendAlert(GPS_STRING);
    }

    @Test
    public void testRelay() throws Exception {

        MinaClient secondConsumerClient = new MinaClient("localhost", PORT, LOGIN_CONSUMER_STRING);

        secondConsumerClient.setMessageHandler(new MessageHandler() {
            @Override
            public void onMessage(Object message) {
                log.info("SECOND:" + message);
            }
        });


        consumerClient.setMessageHandler(new MessageHandler() {
            @Override
            public void onMessage(Object message) {
                log.info("FIRST:" + message);
            }
        });

        producerClient.connect();
        consumerClient.connect();
        secondConsumerClient.connect();
        Thread.sleep(1000);
        producerClient.sendAlert(GPS_STRING);

        Thread.sleep(1000);
    }

}
